import logo from "./logo.svg";
import "./App.css";
import GlassesLayout from "./GlassesComponent/GlassesLayout";

function App() {
  return (
    <div className="App">
      <GlassesLayout />
    </div>
  );
}

export default App;
