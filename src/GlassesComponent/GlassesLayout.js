import React, { Component } from "react";
import { dataGlasses } from "./DataGlasses";
import styles from "./glassesLayout.module.css";
import Header from "./Header";
import Model from "./Model";
import ItemGlasses from "./ItemGlasses";

export default class GlassesLayout extends Component {
  state = {
    glassesArr: dataGlasses,
    itemChoseGlasses: dataGlasses[0],
  };
  renderListGlasses = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <ItemGlasses
          img={item}
          key={index}
          handleChoseGlasses={this.handleChoseGlasses}
        />
      );
    });
  };
  handleChoseGlasses = (item) => {
    this.setState({ itemChoseGlasses: item });
  };
  render() {
    return (
      <div className={`${styles.background} container-fluid`}>
        <Header />
        <Model showChoseItemGlasses={this.state.itemChoseGlasses} />
        <div className="row p-1 mx-3 bg-light">{this.renderListGlasses()}</div>
      </div>
    );
  }
}
