import React, { Component } from "react";
import styles from "./model.module.css";

export default class Model extends Component {
  render() {
    let { url, name, price, desc } = this.props.showChoseItemGlasses;
    return (
      <div className="row">
        <div className={`col-3 mx-auto p-0 ${styles.glassesShadow}`}>
          <div className={styles.glassesCard}>
            <div className={styles.glassesModel}>
              <img src={url} alt="" />
              {/* <img src={require(`${url}`)} alt="" /> */}
            </div>
            <div className={styles.glassesInfo}>
              <h5>{name}</h5>
              <h2>$ {price}</h2>
              <p>{desc}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
