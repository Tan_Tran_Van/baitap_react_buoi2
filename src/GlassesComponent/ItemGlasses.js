import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    return (
      <div className="col-2 p-1" style={{ height: 150 }}>
        <img
          onClick={() => {
            this.props.handleChoseGlasses(this.props.img);
          }}
          className="img-fluid h-100 w-100"
          src={`./glassesImage/g${this.props.img.id}.jpg`}
          // src={require(`./glassesImage/g${this.props.img.id}.jpg`)}
          alt=""
        />
        {/* <img
          className="img-fluid w-100 h-100"
          src={this.props.img.url}
          // src={require(`${this.props.img.url}`)}
          alt=""
        /> */}
      </div>
    );
  }
}
